FROM node:15.13.0-alpine3.10

RUN apk add python3 make build-base

COPY . .

RUN npm install --loglevel verbose
CMD node index.js
