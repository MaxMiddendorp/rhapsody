const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs');
const ytdl = require('ytdl-core');

const {OpusEncoder} = require('@discordjs/opus');

const encoder = new OpusEncoder(48000, 2);

// const encoded = encoder.encode(buffer);
// const decoded = encoder.decode(encoded);


client.on('ready', () => {
    console.log(`Bot started and is logged in as ${client.user.tag}!`);
});

client.on('message', async message => {
        if (!message.guild) return;
        if (message.content === '/help') {
            message.channel.send('helpvak');
        }
        if (message.content === '/join') {
            if (message.member.voice.channel) {
                const connection = await message.member.voice.channel.join();
                connection.voice.setSelfDeaf(true);
            } else {
                message.channel.send('You need to join a voice channel first!');
            }
        }
        if (message.content === '/leave') {
            if (message.member.voice.channel) {
                const connection = await message.member.voice.channel.leave();
            } else {
                message.channel.send('Bot is not in a voice channel!');
            }
        }
        if (message.content === '/play') {
            if (message.member.voice.channel) {
                const connection = await message.member.voice.channel.join();
                connection.play(ytdl('https://www.youtube.com/watch?v=fJ9rUzIMcZQ', { filter: 'audioonly' }));
            } else {
                message.channel.send('You need to join a voice channel first!');
            }
        }
    }
);

client.login(process.env.TOKEN);